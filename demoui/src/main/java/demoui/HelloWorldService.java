package demoui;


import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.glassfish.jersey.server.ChunkedOutput;
import org.glassfish.jersey.server.ManagedAsync;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

@Path("/demo")
public class HelloWorldService {
    
    private static ArrayBlockingQueue buffer = new ArrayBlockingQueue<String>(1024);

    @GET
    @Path("ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String pingpong() {
        String hostname = "unknown";
        try {
            hostname = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e ) {
            e.printStackTrace();
        }
        return "pong from " +  hostname;

    }

    @GET
    @Path("init")
    @Produces(MediaType.TEXT_PLAIN)
    public void initping(String serviceName) {
        while (true) {
            System.out.println("Calling remote server @ 8000");
            //Print a message
            String url = "http://localhost:8000/demo/ping";
            HttpClient client = new HttpClient();
            HttpMethod method = new GetMethod(url);
            try {
                int statusCode = client.executeMethod(method);
                if (statusCode != HttpStatus.SC_OK) {
                    System.err.println("Method failed: " + method.getStatusLine());
                }
                // Read the response body.
                byte[] responseBody = method.getResponseBody();
                // Deal with the response.
                // Use caution: ensure correct character encoding and is not binary data
                String message = UUID.randomUUID() + " : " + new String(responseBody);
                HelloWorldService.buffer.add(message);
                System.out.println(message);
                // Execute the method.
                System.out.println("Sleeping for 2 seconds");
                Thread.sleep(2 * 1000);

            } catch (Exception e) {
                System.err.println("Fatal error: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @GET
    @ManagedAsync
    @Path("log")
    public ChunkedOutput<String> getChunkedResponse() throws Exception {
        final ChunkedOutput<String> output = new ChunkedOutput<String>(String.class);

        new Thread() {
            public void run() {
                try {
                    String chunk;

                    while ((chunk = getNextString()) != null) {
                        output.write(chunk);
                    }
                } catch (IOException e) {
                    // IOException thrown when writing the
                    // chunks of response: should be handled
                    e.printStackTrace();
                } finally {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // simplified: IOException thrown from
                    // this close() should be handled here...
                }
            }
        }.start();

        // the output will be probably returned even before
        // a first chunk is written by the new thread
        return output;
    }

    private String getNextString() {
        // ... long running operation that returns
        //     next string or null if no other string is accessible
        try {
            System.out.println("Reading the log from buffer");
            String consoleLog = HelloWorldService.buffer.take().toString();
            return consoleLog + '\n';
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        return " nothing ";
    }
}